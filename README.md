# Parallel Pizza 3 Slices Calculator

Calculadora dos cortes paralelos equidistantes do centro para separar uma pizza em tres pedacos iguais em funcao do diametro da mesma.

![calculos](./Calculos.png)

## Grafico da funcao encontrada
Grafico da funcao f(x) = 2x + sen(2x) - pi/3


```python
import matplotlib.pyplot as plt
import numpy as np

pizza = lambda x: 2*x + np.sin(2*x) - np.pi/3

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

xs = np.arange(-1, 2, 0.05)
ys = pizza(xs)

ax = plt.gca()
ax.spines['top'].set_color('none')
ax.spines['left'].set_position('zero')
ax.spines['right'].set_color('none')
ax.spines['bottom'].set_position('zero')

line, = plt.plot(xs,ys)
plt.show()
```


![png](tresfatiascalculadora_files/tresfatiascalculadora_1_0.png)


## Resolvendo a equacao
Observamos pelo grafico que a funcao tem apenas uma raiz real.
Resolvendo a equacao 2*x + sin(2*x) - pi/3 = 0, por metodo de Newton-Raphson:

f(x) = 2*x + sin(2*x) - pi/3

f'(x) = 2*cos(2*x) + 2

x0 = 0

xn+1 = xn - f(xn)/f'(xn)

e = 10^(-15)



```python
import math
f = lambda x: 2*x + math.sin(2*x) - math.pi/3
df = lambda x: 2*math.cos(2*x) + 2
err = 0.000000000000001
x0 = 0
while 1:
    x1 = x0 - (f(x0)/df(x0))
    
    if math.fabs(x1-x0) < err:
        break
    
    x0 = x1
    
print(f'x1 = {x1}')
print(f'a = R*{math.sin(x1)}')
print(f'2*a = D*{math.sin(x1)}')
```

    x1 = 0.2681334894944452
    a = R*0.2649320846027768
    2*a = D*0.2649320846027768


## Conclusao 

A constante que relaciona o tamanho do pedaco do meio com o diametro da pizza eh aproximadamente: 0.264932084602776.


## Obrigado!
![thanks](https://pa1.aminoapps.com/6295/23edd485f0d76b15c9659a4ba10d68aec44b1744_hq.gif)
