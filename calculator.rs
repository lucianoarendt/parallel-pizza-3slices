use std::io;

fn main(){
    println!("Insert the pizza radius:");

    let mut r = String::new();

    io::stdin().read_line(&mut r).expect("Failed to read line!");

    let r: f64 = r.trim().parse().expect("Please type a number!");

    let x1: f64 = 2.0*r*0.264932084602776;
    println!("The central piece size should be {x1:.4}!");
}